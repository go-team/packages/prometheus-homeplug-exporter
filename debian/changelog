prometheus-homeplug-exporter (0.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove patches already applied upstream.
  * New patch: Revert alecthomas/kingpin import path.
  * Upstream seems abandoned, switch to my fork of the project.
  * Update Standards-Version with no changes.

 -- Martina Ferrari <tina@debian.org>  Thu, 28 Mar 2024 22:23:54 +0000

prometheus-homeplug-exporter (0.3.0-5) unstable; urgency=medium

  * Team upload
  * Convert patches to gbp-pq format
  * Add new 0002-Support-prometheus-common-v0.50.0.patch (Closes: #1067387)
  * Bump golang-github-prometheus-client-golang-dev version constraint
    (>= 1.19.0)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Thu, 21 Mar 2024 15:34:27 +0000

prometheus-homeplug-exporter (0.3.0-4) unstable; urgency=medium

  * Team upload
  * debconf: add Portuguese translation (Closes: #982810)
  * debconf: add Brazilian Portuguese translation (Closes: #987434)
  * debconf: add Spanish translation (Closes: #988380)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sat, 08 Oct 2022 13:15:02 +0000

prometheus-homeplug-exporter (0.3.0-3) unstable; urgency=medium

  * Team upload
  * Add new 01-use-go-kit-log.patch to build successfully against newer
    golang-github-prometheus-common versions (Closes: #1002268)
  * Update Build-Depends

 -- Daniel Swarbrick <dswarbrick@debian.org>  Fri, 07 Oct 2022 23:52:05 +0000

prometheus-homeplug-exporter (0.3.0-2) unstable; urgency=medium

  [ Martina Ferrari ]
  * Update team email address in Maintainer.
  * Merge minor packaging updates for prom-team.
  * Add French translation to debconf templates. Thanks to Jean-Pierre
    Giraud <jenapierregiraud75@free.fr> for the patch. (Closes: #968117)
  * Add Dutch translation to debconf templates. Thanks to Frans
    Spiesschaert <Frans.Spiesschaert@yucom.be> for the patch. (Closes:
    #968745)
  * Add German translation to debconf templates. Thanks to Helge
    Kreutzmann <debian@helgefjell.de> for the patch. (Closes: #969233)
  * Merge minor packaging updates for prom-team.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Martina Ferrari <tina@debian.org>  Wed, 18 Nov 2020 19:29:04 +0000

prometheus-homeplug-exporter (0.3.0-1) unstable; urgency=medium

  * New upstream release which includes my patch.

 -- Martina Ferrari <tina@debian.org>  Thu, 02 Jul 2020 15:45:59 +0100

prometheus-homeplug-exporter (0.1.0+7-1) unstable; urgency=medium

  * New upstream snapshot, including proper copyright attribution.
  * Update initscript and remove unrelated stuff in rules.
  * Remove unneeded patch, and revert base unit change.
  * Bump debhelper-compat to 13.

 -- Martina Ferrari <tina@debian.org>  Wed, 01 Jul 2020 15:52:19 +0000

prometheus-homeplug-exporter (0.1.0+2-1) unstable; urgency=medium

  * Initial release. Closes: #960795

 -- Martina Ferrari <tina@debian.org>  Sat, 16 May 2020 20:02:58 +0000
